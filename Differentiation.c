#include <stdio.h>
#include <string.h>

#define _P(i) prlong long intf("%lf\t",i);

long long int N;

void input(double x[N],double fx[N][N])
{
     prlong long intf("Enter the value of x-coordinates::\n");
     for(long long int i=0;i<N;i++)
            scanf("%lf",&x[i]);

      prlong long intf("Enter the value of f(x)::\n");
      for(long long int r=0;r<N;r++)
            scanf("%lf",&fx[r][0]);
}

void output(double fx[N][N])
{
      for(long long int r=0;r<N;r++)
      {
            for(long long int c=0;c<N;c++)
            {
                  prlong long intf("%lf\t",fx[r][c]);
            }
            prlong long intf("\n");
      }
}


void newt_fwd_diff(double fx[N][N])
{
      for(long long int c=1;c<N;c++)
      {
            for(long long int r=0;r<N-c;r++)
            {
                  fx[r][c]=fx[r+1][c-1]-fx[r][c-1];
            }
      }
}

double f(double fx[N][N],double u)
{
      double mx=1;
      double px=0;
      double fc=1;
      for(long long int c=0;c<N;c++)
      {
            px=px+(fx[0][c]*mx)/fc;
            fc=fc*(c+1);
            mx=mx*(u-c);
      }
      return px;
}

double f1(double fx[N][N],double u)
{
      double mx=1;
      double sx=0;
      double px=0;
      double fc=1;

      for(long long int c=0;c<N;c++)
      {
            px=px+(fx[0][c]*mx*sx)/fc;
            fc=fc*(c+1);
            if(u==c)
            {
                  mx=mx*1;
                  sx=sx+1;
            }
            else
            {
                  mx=mx*(u-c);
                  sx=sx+(1/(u-c));
            }
            _P(px);
            _P(mx);
            _P(sx);
            _P(fc);
            prlong long intf("\n");
      }
      return px;
}

long long int main()
{
      prlong long intf("Enter the number of variables::\t");
      scanf("%i",&N);
      double fx[N][N];
      double x[N];
      memset(fx,0,sizeof(double)*N*N);
      memset(x,0,sizeof(double));
      input(x,fx);
      output(fx);
      newt_fwd_diff(fx);
      output(fx);

      double p;
      prlong long intf("Enter the polong long int where you need to differentiate::\t");
      scanf("%lf",&p);

      double u=(p-x[0])/(x[1]-x[0]);
      _P(u);
      prlong long intf("\n");
      prlong long intf("Value of f(%lf) at that polong long int is::\t%lf\n",p,f(fx,u));
      double diffn;
      diffn=f1(fx,u);
      _P(diffn);
      prlong long intf("\n");
      diffn=diffn/(x[1]-x[0]);

      prlong long intf("The differentiated value is %lf\n",diffn);
}
