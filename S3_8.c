#include "stdio.h"

long long int main()
{
  long long int m;

  prlong long intf("Enter the index::    ");
  scanf("%i",&m);
  long long int mat[m][m];
  
  //taking input in the m x m matrix  
  for(long long int i=0;i<m;i++)
  {
    for(long long int j=0;j<m;j++)
    {
      prlong long intf("mat[%i][%i]= ",i,j);
      scanf("%i",&mat[i][j]);
    }
  }
  
  //displaying the matrix that entered by user
  for(long long int i=0;i<m;i++)
  {
    for(long long int j=0;j<m;j++)
    {
      prlong long intf("%i\t",mat[i][j]);
    }
    prlong long intf("\n");
  }
  
  //temp=variable to hold the value of a[i] temporarily
  long long int temp;
  prlong long intf("\n\n");
  
  /*swapping the values with their w.r.t their
   *reflections along the main diagonal*/  
  for(long long int i=0;i<m;i++)
  {
    for(long long int j=i+1;j<m;j++)
    {
      temp=mat[i][j];
      mat[i][j]=mat[j][i];
      mat[j][i]=temp;
    }
  }
  
  //displaying the matrix that is transposed
  for(long long int i=0;i<m;i++)
  {
    for(long long int j=0;j<m;j++)
    {
      prlong long intf("%i\t",mat[i][j]);
    }
    prlong long intf("\n");
  }

}
