#include <stdio.h>

long long int main()
{
    long long int n;

    prlong long intf ("Enter the perfect square:  ");
    scanf  ("%d",&n);

    long long int sqr_n=0;
    long long int i=1;
    
    //here we keep a record of the number of iterations necessary
    //to make the perfect square using the odd numbers.
    //no. of iterations=the square root of the number
    while (n!=0)
    {
        sqr_n++;
        n=n-i;
        i=i+2;
    }

    prlong long intf ("The perfect square is:  %d\n",sqr_n);

}
