#include <stdio.h>

//in this function we're going to use recursion for 
//prlong long inting the reverse of digits.
void rev_digits(long long int num)
{
    prlong long intf("%d",num%10);
    if((num/10)!=0)
        rev_digits(num/10);
}

long long int main()
{
    long long int num;
    prlong long intf("Enter your num:  ");
    scanf("%d",&num);
    rev_digits(num);
    prlong long intf("\n");
}
 
