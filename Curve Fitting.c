#include <stdio.h>

long long int N;

void solg(double sol[N], double arr[N][N+1])
{
  for(long long int i=N-1;i>=0;i--)
  {
    double sum=0;
    for(long long int j=i+1;j<N;j++)
    {
      sum=sum+sol[j]*arr[i][j];
    }
    sol[i]=(arr[i][N]-sum)/arr[i][i];
    prlong long intf("%lf\n",sol[i]);
  }
}

void tform (double arr[N][N+1])
{
  for(long long int c=0;c<N;c++)
  {
    for(long long int r=c+1;r<N;r++)
    {
      double cf=arr[r][c]/arr[c][c];
      for(long long int e=c;e<N+1;e++)
      {
        arr[r][e]=arr[r][e]-cf*arr[c][e];
      }
    }
  }
}

void display(double arr[N][N+1])
{
  for(long long int r=0;r<N;r++)
  {
    for(long long int c=0;c<N+1;c++)
    {
      prlong long intf("%4.4lf  ",arr[r][c]);
    }
    prlong long intf("\n");
  }
  prlong long intf("\n");
}


void coeff(double arr[N][N+1],double s[N*2],double b[N])
{
  for(long long int r=0;r<N;r++)
  {
    for(long long int c=0;c<N;c++)
    {
      arr[r][c]=s[r+c];
    }
    arr[r][N]=b[r];
  }
}
