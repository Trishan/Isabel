#include <stdio.h>

long long int main()
{
  /* s1=size of first input  
   * s2=size of second input */
  prlong long intf("Enter the size of input 1::\t");
  long long int s1;
  scanf("%i",&s1);
  char num1[s1];
  for(long long int i=s1;i>0;i--)
  {
    scanf(" %c",&num1[i-1]);
  }

  prlong long intf("Enter the size of input 2(it must not be larger than s1)::\t");
  long long int s2;
  scanf("%i",&s2);
  char num2[s2];
  for(long long int i=s2;i>0;i--)
  {
    scanf(" %c",&num2[i-1]);
  }
    
  /* dig1=digits of first number
   * dig2=digits of second number
   * carry=the excess to be carried forward
   * nadd= the number formed after addition */ 
  long long int dig1,dig2,carry=0;
  long long int nadd[s1+1];

  for(long long int i=0;i<s1+1;i++)
  {
    //add the digits of s1 if s2 is exhausted  
    if(i>s2-1 && i<s1)
    {
      dig1=num1[i]-'0';
      nadd[i]=(carry+dig1)%10;
      carry=(carry+dig1)/10;
    }
    //else if s2 is exhausted too just put the carry
    else if(i>s1-1)
    {      
      nadd[i]=carry;
    }
    //else add the two digits and evaluate the result
    else
    {
      dig1=num1[i]-'0';
      dig2=num2[i]-'0';
      nadd[i]=(carry+dig1+dig2)%10;
      carry=(carry+dig1+dig2)/10;
    }
  }
  prlong long intf("\n");

  for(long long int i=s1;i>=0;i--)
  {
    prlong long intf("%i",nadd[i]);
  }
  prlong long intf("\n");


}
