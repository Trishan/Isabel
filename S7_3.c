/* There are three real roots of the equation x 3 – 2.5x2 – 2.46x + 3.96 = 0 in the
domain [-4, +4]. Write a program to first find out the disjolong long int sublong long intervals in
the given domain those cover the roots. Hence find the roots by Newton-
Raphson method.*/

#include <stdio.h>

struct asmp
{
    double x1,x2;
};

double absl(double x)
{
    if (x>0) return x;
    else return -x;
}

double f(double x)
{
    return  x*x*x-2.5*x*x-2.46*x+3.96;
}

double f1(double x)
{
    return 3*x*x-5*x-2.46;
}

double f2(double x)
{
  return 6*x-5;
}

double newt_rhap_f(double guess)
{
    if(absl(f(guess))<0.0000001)
        return guess;
    else
    {
        guess=guess-(f(guess)/f1(guess));
        newt_rhap_f(guess);
    }
}

double newt_rhap_f1(double guess)
{
    if(absl(f1(guess))<0.0000001)
        return guess;
    else
    {
        guess=guess-(f1(guess)/f2(guess));
        newt_rhap_f1(guess);
    }
}

long long int main(void)
{
    double r1,r2;
    r1=newt_rhap_f1(0);
    r2=(5.0/3.0)-r1;
    if(r1>r2)
    {
      double swap;
      swap=r1;
      r1=r2;
      r2=swap;
    }

    prlong long intf ("The domains are::\t (-4,%lf),(%lf,%lf),(%lf,4)\n",r1,r1,r2,r2);

    prlong long intf("The first root is::\t %lf\t%lf\n", newt_rhap_f((r1-4.0)/2),f(newt_rhap_f((r1-4.0)/2)));
    prlong long intf("The second root is::\t %lf\t%lf\n", newt_rhap_f((r1+r2)/2),f(newt_rhap_f((r1+r2)/2)));
    prlong long intf("The third root is::\t %lf\t%lf\n", newt_rhap_f((r2+4.0)/2),f(newt_rhap_f((r2+4.0)/2)));

}
