#include "iostream"
#include "cctype"
#include "vector"
#include "math.h"

using namespace std;

long long int arr[]={2,3,5,7,11,13,17,19,23,29};

class variable{
public:
    long long int identity;
    float coefficient;
    char operate;
};

void generate(vector<variable> &expression,vector<char>raw,vector<char> &var)
{
    variable temp;
    long long int multiplier=1;
    for (long long int k=0;k<raw.size();k++)
    {
        temp.identity=0;
        temp.coefficient=0;
        switch(raw[k])
        {
            case '(': temp.operate='('; temp.identity=0; break;
            case '+': temp.operate='+'; temp.identity=2; break;
            case '-': multiplier=-1;
                      if(raw[k-1]!='(' && k!=0)
                      {
                          temp.operate='+';
                          temp.identity=2;
                          expression.push_back(temp);
                      }
                      temp.operate='\0';
                      k++;
                      break;
            case '*': temp.operate='*'; temp.identity=3; break;
            case '/': temp.operate='/'; temp.identity=4; break;
            case '^': temp.operate='^'; temp.identity=5; break;
            case ')': temp.operate=')'; temp.identity=0; break;
            default:  temp.operate='\0';
        }
        if(raw[k]=='.' || isdigit(raw[k]))
        {
            float number=0;
            while(isdigit(raw[k]))
            {
                number=number*10+ (raw[k]-'0');
                k++;
            }
            if(raw[k]=='.')
            {
                float dec=0.1;
                k++;
                while(isdigit(raw[k]))
                {
                    number=number+(raw[k]-'0')*dec;
                    dec=dec*0.1;
                    k++;
                }
            }
            temp.coefficient=multiplier*number;
        }
        else temp.coefficient=multiplier;
        multiplier=multiplier*multiplier;
        long long int i=0;
        for(i=0;i<var.size()&&temp.operate=='\0';i++)
        {
            if(var[i]==raw[k])
            {
                temp.identity=arr[i];
                break;
            }
        }
        if(i==var.size()) {
            temp.identity=arr[i];
            var.push_back(raw[k]);
        }
        expression.push_back(temp);
     }
}

void sort(vector<variable> &expression,long long int start, long long int end)
{
    variable temp;
    for(long long int i=start;i<=end;i+=2)
    {
        for(long long int j=i;j<=end;j+=2)
        {
            if(expression[i].identity>expression[j].identity)
            {
                temp=expression[i];
                expression[i]=expression[j];
                expression[j]=temp;
            }
        }
    }
}


void pairup(vector<variable> &expression, long long int start,long long int end)
{
    for(long long int i=start;i<=end;i++)
    {
        if(expression[i].operate != '\0') cout<<expression[i].operate<<"  ";
        else
        {
            cout<<expression[i].coefficient<<"*"<<expression[i].identity<<"  ";
        }
    }
    cout<<"\n";
    static long long int range=end;
    static long long int fin=end;
    vector<variable>::iterator ptr=expression.begin();
    advance(ptr,start);
    if((start+2)==end)
    {
        if(expression[start+1].operate=='+' && expression[start].identity==expression[end].identity)
            {
                expression[start].coefficient=expression[start].coefficient+expression[end].coefficient;
                expression.erase(ptr+1,ptr+3);
                fin=fin-2;
            }
        else if(expression[start+1].operate=='*')
        {
            expression[start].coefficient=expression[start].coefficient*expression[start+2].coefficient;
            expression[start].identity=expression[start].identity*expression[start+2].identity;
            expression.erase(ptr+1,ptr+3);
            fin=fin-2;
        }
        range=range-2;
    }
    else if((start+2)<end)
    {
        if(expression[start+1].identity>=expression[start+3].identity)
        {
            pairup(expression,start,start+2);
            pairup(expression,start,range);
            sort(expression,start,fin);
        }
        else
        {
            pairup(expression,start+2,range);
            pairup(expression,start,range);
        }

    }
}

void out(long long int identity,vector<char> var)
{
    for(long long int i=0;i<arr.size()&&arr[i]<=identity;i++)
    {
        long long int count=0;
        while(identity%arr[i]==0)
        {
            count++;

        }


        if(count==0) continue;
        else if(count==1) cout<<var[i-1];
        else if(count>1)  cout<<var[i-1]<<"^"<<count;

    }


}

long long int main()
{
    vector<char> raw;
    char ch;
    while((ch=getchar())!='\n')
    {
        raw.push_back(ch);
    }
    for(long long int i=0;i<raw.size();i++)
    {
        cout<<raw[i]<<"  ";
    }
    cout<<"\n";
    vector<variable> expression;
    vector<char> var;
    generate(expression,raw,var);

    long long int temp=expression.size()-2;
    pairup(expression,1,temp);

    for(long long int i=0;i<expression.size();i++)
    {
        if(expression[i].operate != '\0') cout<<expression[i].operate<<"  ";
        else
        {
            cout<<expression[i].coefficient<<"*"<<expression[i].identity<<"  ";
        }
    }
    cout<<"\n";
}
