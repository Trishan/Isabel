#include <bits/stdc++.h>
using namespace std;

long long long int N,Q,K;

inline void shift(vector<long long int> &subspace)
{
    long long int N=subspace.size();
    long long int temp=subspace[N-1];
    for(long long int i=N-1;i>0;i--)
        subspace[i]=subspace[i-1];
    subspace[0]=temp;
}

inline long long int maxm(vector<long long int> subspace)
{
    long long int max;
    max=subspace[0];
    for(long long int i=0;i<subspace.size();i++)
        if(subspace[i]>max)
            max=subspace[i];
    return max;
}

long long int main()
{
    /*ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);*/

    cin>>N>>Q>>K;

    vector<long long int> subspace;
    vector<long long int> arr(N);

    for(long long int i=0;i<N;i++)
        cin>>arr[i];

    for(long long int i=0;i<N;)
    {
        long long int count=0;
        while(arr[i]==1)
        {
            count++;
            i++;
        }
        if(count!=0) subspace.push_back(count);
        i++;
    }

    long long int sz;
    sz=subspace.size();
    long long int max=maxm(subspace);
     /*for(long long int i=0;i<Q;i++)
     {
         cout<<max<<"\t";
         for(long long int i=0;i<subspace.size();i++)
            cout<<subspace[i]<<"\t";
        cout<<"\n";
        bool find = false;
        if(max==subspace[0]) max++;
        else if(max==subspace[sz-1]) find=true;
         subspace[0]++;
         subspace[sz-1]--;

         if(max<subspace[0]) max=subspace[0];
         if(find) max=maxm(subspace);

         if(subspace[sz-1]==0)
         {
             shift(subspace);
         }


     }*/




      max=maxm(subspace);
    while(Q-->0)
    {
        char ch;
        cin>>ch;
        if(ch=='?')
        {
            if(K>max)
                cout<<max<<"\n";
            else cout<<K<<"\n";
        }
        else if(ch=='!')
        {
            bool find = false;
            if(max==subspace[0]) max++;
            else if(max==subspace[sz-1]) find=true;
             subspace[0]++;
             subspace[sz-1]--;

             if(max<subspace[0]) max=subspace[0];
             if(find) max=maxm(subspace);

             if(subspace[sz-1]==0)
             {
                 shift(subspace);
             }
        }


    }
}
