#include "iostream"
#include "cctype"
#include "vector"
#include "math.h"

using namespace std;

unsigned long long int arr[]={2,3,5,7,11,13,17,19,23,29};
vector<char> var;

class variable{
public:
    unsigned long long long int identity;
    double coefficient;
    char operate;
};

class index{
public:
    unsigned long long long int begin;
    unsigned long long long int end;
    unsigned long long long int level;
};

void out(unsigned long long long int identity)
{
    for(unsigned long long long int i=0;i<10&&arr[i]<=identity;i++)
    {
        unsigned long long long int count=0;
        while(identity%arr[i]==0)
        {
            count++;
            identity=identity/arr[i];
        }
        if(count==0) continue;
        else if(count==1) cout<<var[i];
        else if(count>1)  cout<<"("<<var[i]<<"^"<<count<<")";
    }
}

void output(vector<variable> expression, unsigned long long long int start, unsigned long long long int end)
{
    for(unsigned long long long int i=start;i<=end;i++)
        {
            if(expression[i].operate != '\0') cout<<expression[i].operate<<"  ";
            else
            {
                if(expression[i].identity==1) cout<<expression[i].coefficient<<" ";
                else {
                    cout<<expression[i].coefficient;
                    out(expression[i].identity);
                    cout<<"  ";
                }
            }
        }
        cout<<"\n";
}

bool is_operator(char ch)
{
    switch(ch)
    {
        case '+':case '-':case'*':case'/':case'^':case '(':case')': return true;
    }
    return false;
}

void generate(vector<variable> &expression,vector<char>raw)
{
    variable temp;
    long long int multiplier=1;
    unsigned long long long int level=0;
    for (unsigned long long long int k=0;k<raw.size();k++)
    {
        temp.identity=0;
        temp.coefficient=0;
        switch(raw[k])
        {
            case '(':
                            {
                                temp.operate='(';
                                temp.identity=level;
                                temp.coefficient=0;
                                level++;
                                break;
                            }
            case '+': temp.operate='+'; temp.identity=2; break;
            case '-': multiplier=-1;
                      if(raw[k-1]!='(' && k!=0)
                      {
                          temp.operate='+';
                          temp.identity=2;
                          expression.push_back(temp);
                      }
                      temp.operate='\0';
                      k++;
                      break;
            case '*':
                        if(raw[k-1]==')'||raw[k+1]==')')
                            continue;
                        else
                        {
                            temp.operate='*';
                            temp.identity=3;
                            break;
                        }

            case '/': temp.operate='/'; temp.identity=4; break;
            case '^': temp.operate='^'; temp.identity=5; break;
            case ')':
                           {
                                level--;
                                temp.operate=')';
                                temp.identity=level;
                                temp.coefficient=0;
                                break;
                            }
            default:  temp.operate='\0';
        }
        if(raw[k]=='.' || isdigit(raw[k]))
        {
            double number=0;
            while(isdigit(raw[k]))
            {
                number=number*10+ (raw[k]-'0');
                k++;
            }
            if(raw[k]=='.')
            {
                double dec=0.1;
                k++;
                while(isdigit(raw[k]))
                {
                    number=number+(raw[k]-'0')*dec;
                    dec=dec*0.1;
                    k++;
                }
            }
            temp.coefficient=multiplier*number;
        }
        else temp.coefficient=multiplier;
        multiplier=multiplier*multiplier;
        if((is_operator(raw[k])&&temp.operate=='\0'))
        {
            temp.identity=1;
            if(k!=raw.size()) k--;
        }
        else
        {
            unsigned long long long int i=0;
                    for(i=0;i<var.size()&&temp.operate=='\0';i++)
                    {
                        if(var[i]==raw[k])
                        {
                            temp.identity=arr[i];
                            break;
                        }
                    }
                    if(i==var.size() && temp.operate=='\0') {
                        temp.identity=arr[i];
                        var.push_back(raw[k]);
                    }
        }
        expression.push_back(temp);
     }
}

void sortex(vector<variable> &expression,unsigned long long long int start, unsigned long long long int end)
{
    variable temp;
    for(unsigned long long long int i=start;i<=end;i+=2)
    {
        for(unsigned long long long int j=i;j<=end;j+=2)
        {
            if(expression[i].identity>expression[j].identity)
            {
                temp=expression[i];
                expression[i]=expression[j];
                expression[j]=temp;
            }
        }
    }
}

void sortin(vector<index> &indexer,unsigned long long long int start, unsigned long long long int end)
{
    index temp;
    for(unsigned long long long int i=start;i<=end;i++)
    {
        for(unsigned long long long int j=i;j<=end;j++)
        {
            if(indexer[i].level<indexer[j].level)
            {
                temp=indexer[i];
                indexer[i]=indexer[j];
                indexer[j]=temp;
            }
            else if(indexer[i].level==indexer[j].level && indexer[i].begin>indexer[j].begin)
            {
                temp=indexer[i];
                indexer[i]=indexer[j];
                indexer[j]=temp;
            }
        }
    }
}

void pairup(vector<variable> &expression, unsigned long long long int start,unsigned long long long int end)
{
    vector<variable>::iterator ptr=expression.begin();
    advance(ptr,start+1);
    for(unsigned long long long int i=start;i+2<=end;)
    {
        if(expression[i+1].operate=='*')
        {
            expression[i].coefficient=expression[i].coefficient+expression[i+2].coefficient;
            output(expression,start,end);
            expression.erase(ptr,ptr+2);
            end=end-2;
            output(expression,start,end);
        }
        else
        {
            i++;
            ptr++;
        }
    }

    sortex(expression,start,end);

    advance(ptr=expression.begin(),start+1);
    for(unsigned long long long int i=start;i+2<=end;)
    {
        if(expression[i+1].operate=='+' && expression[i+2].identity==expression[i].identity)
        {
            expression[i].coefficient=expression[i].coefficient+expression[i+2].coefficient;
            output(expression,start,end);
            expression.erase(ptr,ptr+2);
            end=end-2;
            output(expression,start,end);
        }
        else
        {
            i++;
            ptr++;
        }
    }   
}

void product(vector<variable> &expression,unsigned long long long int start1,unsigned long long long int end1,unsigned long long long int start2,unsigned long long long int end2)
{
    vector<variable>::iterator ptr1=expression.begin();
    vector<variable>::iterator ptr2=expression.begin();
    vector<variable> finale;
    variable temp;
    pairup(expression,start1,end1);
    pairup(expression,start2,end2);
    for(unsigned long long long int i=start1;i<=end1;i+=2)
    {
        for(unsigned long long long int j=start2;j<=end2;j+=2)
        {
            temp.coefficient=expression[i].coefficient*expression[j].coefficient;
            temp.identity=expression[i].identity*expression[j].identity;
            temp.operate='\0';
            finale.push_back(temp);
            temp.coefficient=0;
            temp.identity=2;
            temp.operate='+';
            finale.push_back(temp);
        }
    }

    advance(ptr1,start1);
    advance(ptr2,end2+1);

    expression.erase(ptr1,ptr2);

    finale.pop_back();

    expression.insert(ptr1,finale.begin(),finale.end());   
}

void indexfind(vector<variable> expression, vector<index> &indexer)
{
    indexer.clear();
    index temp;
    for(unsigned long long long int i=0;i<expression.size();i++)
    {
        temp.begin=0;
        temp.end=0;
        if(expression[i].operate=='(')
        {
            temp.begin=i;
            temp.level=expression[i].identity;
            indexer.push_back(temp);
        }

        if(expression[i].operate==')')
        {
            unsigned long long long int j=indexer.size()-1;
            while(indexer[j].end!=0)
                j--;
            indexer[j].end=i;
        }
    }
    sortin(indexer,0,indexer.size()-1);
}

void evaluate(vector<variable> &expression)
{    
    vector<index> indexer;
    indexfind(expression,indexer);
    if(indexer.size()>1)
    {
        if(indexer[0].level==indexer[1].level)
        {
            if((indexer[0].end+1)==indexer[1].begin)
            {
                pairup(expression,indexer[0].begin+1,indexer[0].end-1);
                indexfind(expression,indexer);
                pairup(expression,indexer[1].begin+1,indexer[1].end-1);
                indexfind(expression,indexer);
                product(expression,indexer[0].begin+1,indexer[0].end-1,indexer[1].begin+1,indexer[1].end-1);
                evaluate(expression);
            }
            else
            {
                vector<variable>::iterator ptr1=expression.begin();
                vector<variable>::iterator ptr2=expression.begin();
                vector<variable>::iterator ptr3=expression.begin();
                vector<variable>::iterator ptr4=expression.begin();
                advance(ptr1,indexer[0].begin);
                advance(ptr2,indexer[0].end);
                advance(ptr3,indexer[1].begin);
                advance(ptr4,indexer[1].end);
                expression.erase(ptr4);
                expression.erase(ptr3);
                expression.erase(ptr2);
                expression.erase(ptr1);
                evaluate(expression);
            }
        }
        else
        {
            pairup(expression,indexer[0].begin+1,indexer[0].end-1);
            indexfind(expression,indexer);
            vector<variable>::iterator ptr1=expression.begin();
            vector<variable>::iterator ptr2=expression.begin();
            advance(ptr1,indexer[0].begin);
            advance(ptr2,indexer[0].end);
            expression.erase(ptr2);
            expression.erase(ptr1);
            evaluate(expression);
        }
    }
    else
    {
        pairup(expression,indexer[0].begin+1,indexer[0].end-1);
    }
}

long long int main()
{
    vector<char> raw;
    char ch;
    while((ch=getchar())!='\n')
    {
        raw.push_back(ch);
    }
    for(unsigned long long long int i=0;i<raw.size();i++)
    {
        cout<<raw[i]<<"  ";
    }

    cout<<"\n";
    vector<variable>expression;
    generate(expression,raw);
    for(unsigned long long long int i=0;i<expression.size();i++)
    {
        if(expression[i].operate=='(' || expression[i].operate==')')
        {
            cout<<i<<"  "<<expression[i].operate<<"  "<<expression[i].identity<<"\n";
        }
    }
    evaluate(expression);
    output(expression,0,expression.size()-1);
}
