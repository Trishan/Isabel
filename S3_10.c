#include "stdio.h"

//here the digits are extracted
void extract(long long int a[],long long int i, long long int num)
{
  if(num!=0)
    {
      a[i]=num%10;
      extract(a,i+1,num/10);
    }
}

//here the digits are sorted in ascending order
void sort(long long int a[],long long int sz)
{
  long long int temp;
  for(long long int i=0;i<sz;i++)
  {
    for(long long int j=i+1;j<sz;j++)
    {
      if(a[i]>a[j])
      {
        temp=a[i];
        a[i]=a[j];
        a[j]=temp;
      }
    }
  }
}

//create the number from the digits stored in the array
long long int create(long long int a[],long long int sz)
{
  long long int num=0;
  long long int b=1;
  for(long long int i=0;i<sz;i++)
  {
    num=num+a[sz-i-1]*b;
    b=b*10;
  }
  return num;
}

long long int main()
{
  long long int num;

  prlong long intf("Enter any num in the range of 1-9999 ::\t");
  scanf ("%i",&num);
  prlong long intf("num\tnuma\tnumd\tdiffs\n%4i\t",num);

  long long int as[4]={0},ds[4];
  extract(as,0,num);
  sort(as,4);

  for(long long int i=0;i<4;i++)
      ds[3-i]=as[i];
  
  /*numa=digits are in ascending order
   * numd=digits are in descending order
   * Diffs= difference between the current numa and numd
   * Diffp=difference between the previous numa and numd,
   *        also forms the basis of next iteration */

  long long int diffs,diffp,numa,numd;

  numa=create(as,4);
  numd=create(ds,4);


  prlong long intf("%4i\t",numa);
  prlong long intf("%4i\t",numd);


  diffs=numd-numa;
  prlong long intf("%4i\n",diffs);

  do
  {
    //the previous difference forms the basis of new input in iteration
    diffp=diffs;
    prlong long intf("%4i\t",diffp);
    
    /* as=stores the digits in ascending order
     * ds=stores the digits in descending order */
    long long int as[4]={0},ds[4];

    extract(as,0,diffs);
    sort(as,4);

    for(long long int i=0;i<4;i++)
        ds[3-i]=as[i];

    numa=create(as,4);
    prlong long intf("%4i\t",numa);
    numd=create(ds,4);
    prlong long intf("%4i\t",numd);

    diffs=numd-numa;
    prlong long intf("%4i\n",diffs);
  }
  while(diffs!=diffp); //stop the iteration when successive 
                       //iterations becomes equal

  prlong long intf("The constant is:\t%i\n",diffs);

}
