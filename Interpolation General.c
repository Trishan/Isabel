#include <stdio.h>
#include <string.h>

long long int N;

void coeff_matx(double coeff_mat[N][N],double x[N])
{
      for(long long int c=1;c<N;c++)
      {
            for(long long int r=0;r<(N-c);r++)
            {
                  double d=x[r+c]-x[r];
                  double fd=coeff_mat[r+1][c-1] - coeff_mat[r][c-1];
                  coeff_mat[r][c]=fd/d;
            }
            prlong long intf("\n");
      }
}


void input(double coeff_mat[N][N],double x[N])
{
      prlong long intf("Enter the value of x-coordinates::\n");
      for(long long int i=0;i<N;i++)
            scanf("%lf",&x[i]);
      prlong long intf("Enter the value of corresponding f(x)::\n");
      for (long long int i=0;i<N;i++)
            scanf("%lf",&coeff_mat[i][0]);
}

void output(double arr[N][N])
{
      for(long long int r=0;r<N;r++)
      {
            for(long long int c=0;c<N;c++)
                  prlong long intf("%4.4lf\t",arr[r][c]);
            prlong long intf("\n");
      }
}

double f_fw(double coeff_mat[N][N], double x[N], double a)
{
      double fx=coeff_mat[0][0];
      double s=1;

      for(long long int i=1;i<N;i++)
      {
            s=s*(a-x[i-1]);
            fx=fx+coeff_mat[0][i]*s;
      }
      return fx;
}
long long int main()
{
      prlong long intf("Enter the number of inputs::");
      scanf("%i",&N);
      double coeff_mat[N][N],x[N];
      memset(coeff_mat,0,sizeof(double)*N*N);
      memset(x,0,sizeof(double)*N);

      input(coeff_mat,x);
      output(coeff_mat);

      coeff_matx(coeff_mat,x);
      output(coeff_mat);

      prlong long intf("%lf\n",f_fw(coeff_mat,x,2));


}
