#include <stdio.h>
#include <stdlib.h>

long long int N;

void solg(double sol[N], double arr[N][N+1])
{
  for(long long int i=N-1;i>=0;i--)
  {
    double sum=0;
    for(long long int j=i+1;j<N;j++)
    {
      sum=sum+sol[j]*arr[i][j];
    }
    sol[i]=(arr[i][N]-sum)/arr[i][i];
    prlong long intf("%lf\n",sol[i]);
  }
}

void swap(double arr[N][N+1],long long int r1,long long int r2)
{
      double temp;
      for(long long int c=0;c<N+1;c++)
      {
            temp=arr[r1][c];
            arr[r1][c]=arr[r2][c];
            arr[r2][c]=temp;
      }
}

void tform (double arr[N][N+1])
{
  for(long long int c=0;c<N;c++)
  {
    for(long long int r=c+1;r<N;r++)
    {
      long long int row=c;
      while(arr[row][c]==0 && row<N)
      {
            row++;
      }

      if(row==N) exit(0);
      swap(arr,c,row);
      double cf=arr[r][c]/arr[c][c];
      for(long long int e=c;e<N+1;e++)
      {
        arr[r][e]=arr[r][e]-cf*arr[c][e];
      }
    }
  }
}

void iform (double arr[N][N+1])
{
  for(long long int c=0;c<N;c++)
  {
    for(long long int r=0;r<N;r++)
    {
      if(r==c) continue;
      double cf=arr[r][c]/arr[c][c];
      for(long long int e=0;e<N+1;e++)
        arr[r][e]=arr[r][e]-cf*arr[c][e];
    }
  }
  for(long long int i=0;i<N;i++)
  {
    arr[i][N]/=arr[i][i];
    arr[i][i]=1;
  }

}



void display(double arr[N][N+1])
{
  for(long long int r=0;r<N;r++)
  {
    for(long long int c=0;c<N+1;c++)
    {
      prlong long intf("%4.4lf  ",arr[r][c]);
    }
    prlong long intf("\n");
  }
  prlong long intf("\n");
}

void input(double arr[N][N+1])
{
  for(long long int r=0;r<N;r++)
  {
    for(long long int c=0;c<N+1;c++)
    {
      scanf("%lf",&arr[r][c]);
    }
  }
}

long long int main()
{
  prlong long intf("The number of variables??\t");
  scanf("%i",&N);
  double arr[N][N+1];
  input(arr);
  display(arr);
  tform(arr);
  display(arr);
  display(arr);

  /*iform(arr);
  display(arr);*/
  double sol[N];
  solg(sol,arr);
}
