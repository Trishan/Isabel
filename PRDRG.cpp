#include <iostream>
using namespace std;

long long int gcd(unsigned long long long int x,unsigned long long long int y)
{
    long long int temp;
    while((x%y)!=0)
    {
        temp=y;
        y=x%y;
        x=temp;
    }
    return y;
}

long long int main()
{
    long long int T;
    cin>>T;
    while(T-->0)
    {
        long double x=1,y=1;
        long double l=0;
        long double r=1;
        long long int N;
        cin>>N;
        for(long long int i=1;i<=N;i++)
        {
            if(i%2==1) r=(r+l)/2;
            else l=(l+r)/2;
        }
        if(N%2==1)
        {
            x=r;
            while((long long int)x!=x)
            {
                x=x*10;
                y=y*10;
            }
            long long int g=gcd((long long int)x,(long long int)y);
            x=x/g;
            y=y/g;
            cout<<x<<" "<<y<<" ";
        }
        else
        {
            x=l;
            while((long long int)x!=x)
            {
                x=x*10;
                y=y*10;
            }
            long long int g=gcd((long long int)x,(long long int)y);
            x=x/g;
            y=y/g;
            cout<<x<<" "<<y<<" ";
        }
    }
}
