#include "stdio.h"
#include "stdlib.h"

struct Node
{
  long long int data;
  struct Node *address;
};

// Creating a node
void create(long long int val, struct Node **head)
{
  struct Node *temp=(struct Node*)malloc(sizeof(struct Node));
  /*We created a polong long inter which polong long ints to the memory location( returned by    
   *malloc) which is Node datatype. */
  temp->data=val;
  /* access the structure member data of the variable polong long inted by temp
   * and store data there */ 
  temp->address=(*head);
  /* access the struct member address of the variable polong long inted by temp
   * and store the location of the next polong long inter */
  (*head)=temp; // make the node as the new head
}

//function to output our linked list
void console_o(struct Node *n)
{
  while(n!=0)
  {
    prlong long intf("%i\t",n->data);
    n=n->address; // after prlong long inting data go to the new node
  }
  prlong long intf("\n");
}

//insert an element after node n
void insert(struct Node **n,long long int val)
{
  struct Node *temp=(struct Node*)malloc(sizeof(struct Node));
  temp->address=(*n)->address; 
  /* make the address stored inside the variable the location 
   * of next node */
  temp->data=val;
  (*n)->address=temp;
  /* update the location of the n to
   * the location of temp */ 
}

//function to delete any node after head
void delete(struct Node **n)
{
  struct Node *temp=(struct Node*)malloc(sizeof(struct Node));
  
  /* Skip the immediate node after n and 
   * store the location of that node in polong long inter temp */
  temp=(*n)->address;
  temp=temp->address;
  
  //update n's address to the location polong long inted by temp
  (*n)->address=temp;
}

//Reversing the whole list 
void reverse(struct Node **head)
{
  /* prev polong long ints to the location of last node accessed
   * current is polong long inting the location of current node
   * next is polong long inting to the location of the next node */  
  struct Node *prev=NULL;
  struct Node *current=*head;
  struct Node *next=NULL;
  
  while(current!=NULL)
  {      
    next=current->address; 
    current->address=prev; //changing the address of the polong long inter from next to prev
    prev=current;          //updating the prev
    current=next;          //updating the current
  }
  *head=prev;
}



long long int main()
{
  char more;
  long long int val;
  struct Node *head=(struct Node*)malloc(sizeof(struct Node));
  prlong long intf("Enter the value: ");
  scanf("%i",&val);
  head->data=val;
  head->address=0;
  prlong long intf("Do you wanna input more data: Input 'y' for yes:\t");
  scanf(" %c",&more);
  while(more=='y')
  {
    prlong long intf("Enter the value: ");
    scanf("%i",&val);
    create(val,&head);
    console_o(head);

    prlong long intf("More data::\t");
    scanf(" %c",&more);
  }
  
  prlong long intf("Reversing the order::\n");
  reverse(&head);
  console_o(head);

  char want;
  prlong long intf("Do you want to add more data? Press 'y' for yes::\t");
  scanf(" %c",&want);


  while(want=='y')
  {
    long long int pos;
    prlong long intf("Enter the position::\t");
    scanf("%d",&pos);

    struct Node *n=head;
    while(pos-->0)
      n=n->address;

    long long int val;
    prlong long intf("Enter the value::\t");
    scanf("%i",&val);
    insert(&n,val);

    console_o(head);

    prlong long intf("More data::\t");
    scanf(" %c",&want);
  }

  prlong long intf("Do you want to delete nodes?::\t");
  scanf(" %c",&want);

  while(want=='y')
  {
    long long int pos;
    prlong long intf("Enter the position::\t");
    scanf("%d",&pos);

    struct Node *n=head;
    while(pos-->1)
      n=n->address;
    delete(&n);
    console_o(head);

    prlong long intf("Do you want to delete nodes?::\t");
    scanf(" %c",&want);
  }
}
