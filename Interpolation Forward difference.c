#include <stdio.h>
#include <string.h>

long long int N;

long long int coeff_create(double coeff_matx[N][N])
{
      for(long long int c=1;c<N;c++)
      {
            long long int bool=1;
            for(long long int r=0;r<N-c;r++)
            {
                  coeff_matx[r][c]=coeff_matx[r+1][c-1]-coeff_matx[r][c-1];
                  if(coeff_matx[r][c]==coeff_matx[0][c]) bool=bool*1;
                  else bool=bool*0;
            }
            if(bool) return c;
      }
      return N;
}

void input(double coeff_matx[N][N],double x[N])
{
      prlong long intf("Enter the x co-ordinates::\n");
      for(long long int i=0;i<N;i++)
      {
            scanf("%lf",&x[i]);
      }
      prlong long intf("Prlong long int the corresponding f(x) co-ordinates\n");
      for(long long int i=0;i<N;i++)
      {
            scanf("%lf",&coeff_matx[i][0]);
      }
}

void output(double coeff_matx[N][N])
{
      for(long long int r=0;r<N;r++)
      {
            for(long long int c=0;c<N;c++)
            {
                  prlong long intf("%4.4lf  ",coeff_matx[r][c]);
            }
            prlong long intf("\n");
      }
}

long long int main()
{
      prlong long intf("Enter the number of inputs::\t");
      scanf("%i",&N);

      double coeff_matx[N][N],x[N];
      memset(coeff_matx,0,sizeof(double)*N*N);

      input(coeff_matx,x);
      long long int c=coeff_create(coeff_matx);
      output(coeff_matx);

      double var;
      prlong long intf("Enter value of x::\t");
      scanf("%lf",&var);

      double fx=0;
      double pl=1;
      double u;

      if(var>(x[N-1]-x[0])/2)
      {
            u=(var-x[N-1])/(x[1]-x[0]);
            for(long long int i=0;i<=c;i++)
            {
                  fx=fx+coeff_matx[N-i-1][i]*pl;
                  pl=pl*(u+i);
                  pl=pl/(i+1);
                  prlong long intf("%lf\n",fx);
            }
      }
      else
      {
            u=(var-x[0])/(x[1]-x[0]);
            for(long long int i=0;i<=c;i++)
            {
                  fx=fx+coeff_matx[0][i]*pl;
                  pl=pl*(u-i);
                  pl=pl/(i+1);
                  prlong long intf("%lf\n",fx);
            }
      }

      prlong long intf("Approximated value::\t%lf\n",fx);
}
