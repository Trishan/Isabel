#include <stdio.h>
#include <stdlib.h>
#include <string.h>

long long int N;

double absl(double x)
{
      if(x>0) return x;
      else return -x;
}

void gauss_siedel(double arr[N][N+1])
{
      double sol[N];
      memset(sol,0,N*sizeof(double));
      double temp=0;

      for (long long int i=0;i<10;i++)
      {
            temp=sol[0];
            for(long long int r=0;r<N;r++)
            {
                  double sum=0;
                  for(long long int c=0;c<N;c++)
                  {
                        if(c!=r) sum=sum+arr[r][c]*sol[c];
                  }
                  sol[r]=(arr[r][N]-sum)/arr[r][r];
                  prlong long intf("%lf\t",sol[r]);

            }
            prlong long intf("\n");
      }
}

void input(double arr[N][N+1])
{
      for(long long int r=0;r<N;r++)
      {
            for(long long int c=0;c<N+1;c++)
            {
                  scanf("%lf",&arr[r][c]);
            }
      }
}

void output(double arr[N][N+1])
{
      for(long long int r=0;r<N;r++)
      {
            for(long long int c=0;c<N;c++)
                  prlong long intf("%4.4lf\t",arr[r][c]);
            prlong long intf("\n");
      }

}

long long int main()
{
      prlong long intf("Enter the number of elements::\t");
      scanf("%i",&N);

      double arr[N][N+1];

      input(arr);
      output(arr);
      gauss_siedel(arr);

}
