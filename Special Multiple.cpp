#include<iostream>
#include<vector>
#include <cmath>
using namespace std;

long long int num;

long long int dynamic(vector<long long int> Modulo,long long int n,long long int sum, long long int nu)
{
    /*cout << sum<< "\t"<< nu<<"\n";*/

    if(sum%num==0)
    {
        return nu*pow(10,n);
    }
    else if(n==0 && sum%num!=0) return 0;
    else
    {
        long long int u=dynamic(Modulo,n-1,sum,nu*10);
        long long int v=dynamic(Modulo,n-1,sum-Modulo[n-1],nu*10+9);

        if(u!=0 && v!=0)
        {
            if(u<v) return u;
            else return v;
        }
        else return u+v;

    }
    return 0;
}

int main()
{
    long long int t,o;
    cin>>t;
    while(t--)
    {
        vector<long long int> Modulo;
        cin>>num;
        long long int c=9;
        do
        {
            /*cout<<"\t\t"<<c<<"\n";*/
            if(c%num==0)
            {
                o=c;
                break;
            }
            Modulo.push_back(c%num);
            c=c*10;
            o=dynamic(Modulo,Modulo.size()-1,num-Modulo[Modulo.size()-1],9);
        }
        while(!o);

        cout<<o<<"\n";

    }
}
