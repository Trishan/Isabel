#include <bits/stdc++.h>
using namespace std;

typedef long long long long int ll;
typedef long double ld;


long long int main()
{
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    long long int n;
    cin>>n;
    ll a[n];

    for(long long int i=0;i<n;i++)
        cin>>a[i];

    long long int count=0;
    for(long long int i=0;i<n-1;i++)
    {
        count++;
        for(long long int j=i+2;j<n;j++)
        {
            if((a[j]-a[i])>(a[i+1]-a[i])*(j-i)) count++;
        }
    }
    cout<<count;
}
