#include <stdio.h>
#define no_of_bits 8*sizeof(unsigned long long int)

long long int main()
{
  unsigned long long int x,y,z=0;

  prlong long intf("Enter the value of x::\t");
  scanf("%du",&x);
  prlong long intf("Enter the value of y::\t");
  scanf("%du",&y);

  unsigned long long int mask=1;

  for(long long int i=1;i<=(no_of_bits)/2;i++)
  {
    if((x&mask)>0) z=z|mask;
    mask = mask<<1;
    if((y&mask)>0) z=z|mask;
    mask = mask<<1;
  }

  prlong long intf("The new long long integer is::\t%i\n",z);

}
