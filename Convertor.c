#include <stdio.h>

char out(long long int n)
{
	switch(n)
	{
		case 0: return '0';
		case 1: return '1';
		case 2: return '2';
		case 3: return '3';
		case 4: return '4';
		case 5: return '5';
		case 6: return '6';
		case 7: return '7';
		case 8: return '8';
		case 9: return '9';
		case 10: return 'a';
		case 11: return 'b';
		case 12: return 'c';
		case 13: return 'd';
		case 14: return 'e';
		case 15: return 'f';
	}
}

long long int inp(char a)
{
	switch(a)
	{
		case '0': return 0;
		case '1': return 1;
		case '2': return 2;
		case '3': return 3;
		case '4': return 4;
		case '5': return 5;
		case '6': return 6;
		case '7': return 7;
		case '8': return 8;
		case '9': return 9;
		case 'a': return 10;
		case 'b': return 11;
		case 'c': return 12;
		case 'd': return 13;
		case 'e': return 14;
		case 'f': return 15;
	}
}

void prlong long int(long long int n, long long int t)
{
	if(n/t!=0)
	prlong long int(n/t,t);
	prlong long intf("%c",out(n%t));
}

long long int input(long long int ch)
{
	prlong long intf("Enter the number of digits: ");
	long long int n;
	scanf("\n");
	scanf("%d",&n);

	char a[n];
	long long int i;
	long long int b=1;
	long long int num=0;
  scanf("\n");
	for(i=n-1; i>=0; i--)
	    {
				 scanf("%c",&a[i]);
			 }

  for(i=0; i<n; i++)
	{
		long long int temp = inp(a[i]);
		num=num+ temp*b;
		b=b*ch;
	}

	return num;

}

long long int main()
{
	long long int tp;
	prlong long intf("Enter the type of input:\n 16 for hex\n 2 for binary\n 8 for octal\n 10 for decimal::  ");
	scanf("%d",&tp);

	long long int n=input(tp);

	prlong long intf("Enter the type of output:\n 16 for hex\n 2 for binary\n 8 for octal\n 10 for decimal::  ");
	long long int ch;
	scanf("%d",&ch);

	switch (ch) {
		case 2: prlong long int(n,2); break;
		case 16: prlong long int(n,16);  break;
		case 8: prlong long int(n,8);  break;
        case 12: prlong long int(n,12); break;
    	case 10: prlong long intf("%d", n);
	}

}
