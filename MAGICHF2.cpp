#include <iostream>
#include <iomanip>
#include <math.h>
using namespace std;

long long int main()
{
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    long long int T;
    cin>>T;
    while(T-->0)
    {
        long unsigned long long int N;
        long unsigned long long int t;
        cin >> N>>t;
        float p;
        for(long long int i=1;i<=t;i++)
        {
            if(N<=2)
            {
                N=1;
                break;
            }
            else if(N%i==0) N=(2*N)/3;
            else if(N%i==1) N=2*(N/3);
            else if(N%i==2) N=2*(N+1)/3;
        }
        p=1/(float)N;
        prlong long intf("%0.6f\n",p);
    }
}
