#include <stdio.h>

double f(double x)
{
    return 3*x*x+2*x-9;
}

double f1(double x)
{
    return 6*x+2;
}

double pos(double x)
{
    if(x<0) return -1*x;
    else return x;
}

double newt_rhap(double guess)
{
    if(pos(f(guess))<0.0001)
        return guess;
    else
    {
        prlong long intf("%lf\t%lf\t",f(guess),f1(guess));
        guess=guess-(f(guess)/f1(guess));
        prlong long intf("%lf\n",guess);
        newt_rhap (guess);
    }
}

long long int main()
{
    prlong long intf("Enter your initial guess::\t");
    double guess;
    scanf("%lf",&guess);
    prlong long intf("%lf\n",newt_rhap(guess));
}
    
    
