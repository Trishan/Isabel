#include "iostream"
#include "cctype"
#include "vector"

using namespace std;

class data
{
public:
    float num;
    char  ch;
};

void generate(vector<data> &expression,vector<char> input)
{
    long long int k=0;
    while(k<input.size())
    {
        data expr;
        if(!(isdigit(input[k])) && (input[k]!='.'))
        {
            expr.ch=input[k];
            switch(input[k])
            {
                case '+': expr.num=1;
                          break;
                case '-': expr.num=0;
                          break;
                case '*': expr.num=2;
                          break;
                case '/': expr.num=3;
                          break;
            }
            expression.push_back(expr);
            k++;
        }
        if(input[k]=='.' || isdigit(input[k]))
        {
            float number=0;
            while(isdigit(input[k]))
            {
                number=number*10+ (input[k]-'0');
                k++;
            }
            if(input[k]=='.')
            {
                float dec=0.1;
                k++;
                while(isdigit(input[k]))
                {
                    number=number+(input[k]-'0')*dec;
                    dec=dec*0.1;
                    k++;
                }
            }
            expr.num=number;
            expr.ch='\0';
            expression.push_back(expr);
        }
    }
}

float binary(float val1, float val2, char ch)
{
    switch(ch)
    {
        case '+': return val1+val2;
        case '-': return val1-val2;
        case '*': return val1*val2;
        case '/': if(val2==0)
                    {
                        cout<<"Fucker\n";
                        exit(0);
                    }
                    else return val1/val2;
    }
}

float val(vector<data> expression,long long int index)
{
    if(expression[index+2].ch=='(') return binary(expression[index].num,val(expression,index+3),expression[index+1].ch);
    else if((index+3)<expression.size() && expression[index+3].ch!=')')
    {
        if(expression[index+3].num > expression[index+1].num)
            return binary(expression[index].num,val(expression,index+2),expression[index+1].ch);
        else return binary(expression[index].num,expression[index+2].num,expression[index+1].ch);
    }
    else return binary(expression[index].num,expression[index+2].num,expression[index+1].ch);
}


long long int main()
{
    vector<char> input;
    char ch;
    while((ch=getchar())!='\n')
    {
        input.push_back(ch);
    }
    vector<data> expression;
    generate(expression,input);
    for(long long int i=0;i<expression.size();i++)
    {
        if(expression[i].ch=='\0') cout<<expression[i].num;
        else cout<<expression[i].ch;
    }

    cout<<"\t"<<val(expression,0);
}
