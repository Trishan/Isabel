#include <stdio.h>

//here we calculate the sum of the factors
long long int factor_sum(long long int n)
{
   long long int sum=0;
   for(long long int i=1;i*i<=n;i++)
   {
       if(n%i==0)
       {
          if(i*i!=n) sum=sum+i+(n/i);
          else sum=sum+i;
       }
   }

   return sum;
}

long long int main()
{
    long long int n,sum=0 ;

    prlong long intf ("Enter the range of factor sum summation:  ");
    scanf  ("%d",&n);
    
    //here we iterate to find the summation of the numbers
    while(n>0)
        sum=sum+factor_sum (n--);

    prlong long intf ("The summation of the series is: %d\n",sum);

    return 0;
}
