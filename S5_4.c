#include "stdio.h"


struct student{
  long long int roll;

  float economics;
  float statistics;
  float mathematics;
  float comp_sci;
  float literature;
  float philosophy;

  float average;
};

// Calculating the average
void average(struct student *std)
{
  float av=(*std).economics+(*std).statistics+(*std).mathematics+(*std).comp_sci
            +(*std).literature+(*std).philosophy;
  (*std).average=av/6;
}

//sorting the array on the basis of average marks
void b_sort(struct student student_list[],long long int no_inp)
{
  struct student temp;
  for(long long int i=0;i<no_inp;i++)
  {
    for(long long int j=i+1;j<no_inp;j++)
    {
        if(student_list[i].average>student_list[j].average)
        {
          temp=student_list[i];
          student_list[i]=student_list[j];
          student_list[j]=temp;
        }
    }
  }
}


long long int main()
{
  long long int no_inp;
  prlong long intf("Enter the number of inputs:\t");
  scanf("%i",&no_inp);

  struct student student_list[no_inp];

  for(long long int i=0;i<no_inp;i++)
  {
    prlong long intf("Input the %i student data::\n",i+1);

    prlong long intf("Input the roll number::\t");
    scanf("%i",&student_list[i].roll);
    prlong long intf("Input the economics marks::\t");
    scanf("%f",&student_list[i].economics);
    prlong long intf("Input the statistics marks::\t");
    scanf("%f",&student_list[i].statistics);
    prlong long intf("Input the comp_sci marks::\t");
    scanf("%f",&student_list[i].comp_sci);
    prlong long intf("Input the mathematics marks::\t");
    scanf("%f",&student_list[i].mathematics);
    prlong long intf("Input the literature marks::\t");
    scanf("%f",&student_list[i].literature);
    prlong long intf("Input the philosophy marks::\t");
    scanf("%f",&student_list[i].philosophy);

    average(&student_list[i]);
  }

  b_sort(student_list,no_inp);

  for(long long int i=0;i<no_inp;i++)
  {
    prlong long intf("%i\n",student_list[i].roll);
  } 

}
