/*Write an algorithm to determine maximum of three numbers. Also draw the corresponding flow chart.*/

#include <stdio.h>

long long int main()
{
    long long int N;
    prlong long intf("Enter the number of elements: ");
    scanf ("%d",&N);

    long long int arr[N];

    for(long long int i=0;i<N;i++)
    {
        scanf("%d",&arr[i]);   //here the array elements are initialized
    }                          //to their respective values

    long long int max=arr[0];

    for(long long int i=0; i<N;i++)
    {
        if(arr[i]>max)      //here we're updating the max element
            max=arr[i];
    }

    prlong long intf("max= %d\n",max);
}
