#include <stdio.h>

unsigned char mask=128;

//function to prlong long int the number in binary
void prlong long int(long long int num)
{
  long long int t=mask;
  while(t!=0)
  {
    if((t&num)>0) prlong long intf("1");
    else prlong long intf("0");
    t=t>>1;
  }
  prlong long intf("\n");
}

//function to recursively shift and add bits
void shift(long long int num, long long int n)
{
  if((num&1)>0)
  {
    num=num>>1;
  }
  else
  {
    num=num>>1;
    num=num|mask;
  }

  if(--n>0) shift (num,n);
  else prlong long int(num);
}

long long int main()
{
  unsigned long long int x;
  prlong long intf("Enter the number(0-255)::\t");
  scanf("%i",&x);

  while((x&mask)<=0 && mask!=0)
  {
    mask=mask>>1;
  }

  long long int n;
  prlong long intf("Enter the number of bits to shift::\t");
  scanf("%i",&n);

  shift(x,n);
}
