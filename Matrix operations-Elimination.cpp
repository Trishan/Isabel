#include "iostream"
#include "iomanip"
#include "vector"
using namespace std;


void output(vector<vector<float>> matrix)
{
    for(long long int r=0;r<matrix.size();r++)
    {
        for(long long int c=0;c<matrix[r].size();c++)
            {
                cout<<setw(7)<<setprecision(6);
                cout<<matrix[r][c]<<"\t";
            }
        cout<<"\n";
    }
    cout<<"\n\n";
}

void input(vector<vector<float>> &matrix)
{
    for(long long int r=0;r<matrix.size();r++)
        for(long long int c=0;c<matrix[r].size();c++)
            cin>>matrix[r][c];
}

void pivot(vector<vector<float>> &matrix,long long int i)
{
    float coeff;
    for(long long int r=i+1;r<matrix.size();r++)
    {
        coeff=matrix[r][i]/matrix[i][i];
        for(long long int c=i;c<matrix[i].size();c++)
            matrix[r][c]=matrix[r][c]-coeff*matrix[i][c];
    }
    output(matrix);
}

bool swap_r(vector<vector<float>> &matrix, long long int i)
{
    bool consistency=false;
    for(long long int r=i+1;r<matrix.size();r++)
    {
        if(matrix[r][i]!=0)
        {
            matrix[i].swap(matrix[r]);
            consistency=true;
            break;
        }
    }
    output(matrix);
    return consistency;
}

bool eliminate(vector<vector<float>> &matrix)
{
    for(long long int i=0;i<matrix.size();i++)
    {
        bool consistency=true;
        if(matrix[i][i]==0) consistency=swap_r(matrix,i);
        if(consistency) pivot(matrix,i);
        else return consistency;
    }
    return true;
}

void back_substt(vector<vector<float>> matrix)
{
    vector<float> var(matrix.size());
    float sum=0;

    for(long long int i=var.size()-1;i>=0;i--)
    {
        for(long long int c=i+1;c<matrix.size();c++)
            sum=sum+matrix[i][c]*var[c];
        var[i]=(matrix[i][matrix.size()]-sum)/matrix[i][i];
        sum=0;
    }

    for(long long int i=0;i<var.size();i++)
        cout<<var[i]<<"\n";
}





long long int main()
{
    long long int row,column;
    cout<<"Enter the number of rows and column:   ";
    cin>>row>>column;
    
    vector<vector<float>> matrix(row,vector<float>(column,0));
    input(matrix);

    bool consistent=eliminate(matrix);

    if(consistent) back_substt(matrix);
    else cout<<"The equations are inconsistent\n";
}
