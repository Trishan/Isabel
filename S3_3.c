#include "stdio.h"

//we take input in a 3x3 matrix
void input(long long int mat[3][3])
{
  for(long long int i=0;i<3;i++)
  {
    for(long long int j=0;j<3;j++)
    {
      prlong long intf("mat[%i][%i]= ",i+1,j+1);
      scanf("%i",&mat[i][j]);
    }
  }
}

//we display the 3x3 matrix
void output(long long int mat[3][3])
{
  for(long long int i=0;i<3;i++)
  {
    for(long long int j=0;j<3;j++)
    {
      prlong long intf("%4i ",mat[i][j]);
    }
    prlong long intf("\n");
  }
  prlong long intf("\n\n");
}

/* Finding the sum of two matrix
 * we find the sum iteratively between two 
 * corresponding elements */
void sum(long long int mat1[3][3],long long int mat2[3][3])
{
  for(long long int i=0;i<3;i++)
  {
    for(long long int j=0;j<3;j++)
    {
      mat1[i][j]=mat1[i][j]+mat2[i][j];
    }
  }
  output(mat1);
}

/*Substracting mat1[][]-mat2[][]
 * the same way is addition */
void substract(long long int mat1[3][3],long long int mat2[3][3])
{
  for(long long int i=0;i<3;i++)
  {
    for(long long int j=0;j<3;j++)
    {
      mat1[i][j]=mat1[i][j]-mat2[i][j];
    }
  }
  output(mat1);
}

/*Multiplying (row x column)
 * using 3 iterations to multiply traditionally */
void product(long long int mat1[3][3],long long int mat2[3][3])
{
  long long int mat[3][3];
  for(long long int r=0;r<3;r++)
  {
    for(long long int c=0;c<3;c++)
    {
      mat[r][c]=0;
      for(long long int i=0;i<3;i++)
      {
        mat[r][c]=mat[r][c]+mat1[r][i]*mat2[i][c];
      }
    }
  }
  output(mat);
}

long long int main() {
  char ch;
  prlong long intf("Enter what you wanna do with the matrices\n"
         "Enter s for sum\n"
         "Enter d for substraction(malong long intain order of d=A-B\n"
         "Enter p for product(malong long intain order of p=A*B\n");
  scanf(" %c",&ch);


  prlong long intf("Enter the matrix A::\n");
  long long int mat1[3][3];
  input(mat1);
  prlong long intf("The matrix entered is\n");
  output(mat1);

  prlong long intf("Enter the matrix B::\n");
  long long int mat2[3][3];
  input(mat2);
  prlong long intf("The matrix entered is\n");
  output(mat2);



  switch(ch)
  {
    case 's': sum(mat1,mat2); break;
    case 'd': substract(mat1,mat2); break;
    case 'p': product(mat1,mat2); break;
    default: prlong long intf("Input invalid\n");
  }


}
