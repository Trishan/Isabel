#include <iostream>
#include <list>

using namespace std;

//Creating a graph Utility
class Graph{
    //Creating an object of type Vertex
    class Vertex{
    public:
        long long int val;
        bool visited;
        //Contructor to set visited flag to 0
        Vertex()
        {
            visited=false;
            val=0;
        }
    };
    //Number of vertex required
    long long int V;
    //Polong long inter to *array* of Adjecency lists
    list<Vertex> *adj;
    void DFSUtil(Vertex v)
    {
        v.visited=true;
        cout<<v.val;
        list<Vertex>::iterator i;
        for(long long int j=0;j<V;i++)
        {
        for(i=adj[j].begin(); i!=adj[j].end();i++)
                cout<<i->val;
        cout<<"\n";
    }
    }
public:
    Graph(long long int V)
    {
        this->V=V;
        adj=new list<Vertex> [V];
    }
    void addEdge(long long int u,long long int v)
    {
        Vertex X,Y;
        Y.val=v;
        adj[u].push_back(Y);
    }
    void DFS(long long int v)
    {
        Vertex x;
        x.val=v;
        DFSUtil(x);
    }


};

long long int main()
{
    // Create a graph given in the above diagram
 Graph g(4);
 g.addEdge(0, 1);
 g.addEdge(0, 2);
 g.addEdge(1, 2);
 g.addEdge(2, 0);
 g.addEdge(2, 3);
 g.addEdge(3, 3);

 cout << "Following is Depth First Traversal"
         " (starting from vertex 2) \n";
 g.DFS(1);

 return 0;
}
