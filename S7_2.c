/*Use the above program to find 3 roots of the equation xtan(x)=c where c is a
user-input constant. Use both bisection method and Newton-Raphson
method.*/


#include <stdio.h>
#include <math.h>


double c;

struct asmp
{
    double x1,x2;
};

double absl(double x)
{
    if (x>0) return x;
    else return -x;
}

double f(double x)
{
    return x*tan(x)-c;
}

double f1(double x)
{
    return x/(pow(cos(x),2))+tan(x);
}
double bisect(struct asmp ass)
{
    if(f(ass.x1)*f(ass.x2)>0) return 0;
    double mid;
    while(absl(ass.x2-ass.x1)>0.0001)
    {
        mid=(ass.x1+ass.x2)/2;
        prlong long intf ("%lf\n",mid);
        if(f(ass.x1)*f(mid)<0)
            ass.x2=mid;
        else if(f(mid)*f(ass.x2)<0)
            ass.x1=mid;
        else return mid;
    }
    return mid;
}

double newt_rhap(double guess)
{
    if(f(guess)<0.0000001)
        return guess;
    else
    {
        guess=guess-(f(guess)/f1(guess));
        newt_rhap (guess);
    }
}

double reg_fal(struct asmp ass)
{
    if(f(ass.x1)*f(ass.x2)>0) return 0;
    double mid;
    while(absl(ass.x2-ass.x1)>0.0001)
    {
        mid= (ass.x1*f(ass.x2)-ass.x2*f(ass.x1))/(f(ass.x2)-f(ass.x1));
        prlong long intf ("%lf %lf\n",mid,absl (ass.x2-ass.x1));
        if(f(ass.x1)*f(mid)<0)
            ass.x2=mid;
        else if(f(mid)*f(ass.x2)<0)
            ass.x1=mid;
        else return mid;
    }
    return mid;
}



long long int main(void)
{
    double c,guess;
    struct asmp ass;

    prlong long intf ("Enter the value of c:\t ");
    scanf ("%lf",&c);

    prlong long intf ("Enter your assumptions: \t");
    scanf  ("%lf %lf",&ass.x1,&ass.x2);

    prlong long intf ("Value by Bisection:\t %lf\nValue by Regula falsi:\t %lf\n",
            bisect (ass),reg_fal (ass));

    prlong long intf ("Your initial guess:\t ");
    scanf  ("%lf",&guess);

    prlong long intf ("Value by Newton Rhapson is:\t %lf\n",newt_rhap (guess));
    return 0;
}
