#include "iostream"
#include "vector"
using namespace std;

class arr
{
public:
    long long int value;
    bool choosen;
};

void permute(vector<arr> p,long long int j)
{
    static vector<long long int> o(p.size());
    if(j==p.size())
    {
        for(long long int k=0;k<o.size();k++)
            cout<<o[k];
            cout<<"\n";
            return;
    }

    for(long long int i=0;i<p.size();i++)
    {
        if(p[i].choosen==true) continue;
        else
        {
                o[j]=p[i].value;
                p[i].choosen=true;
                permute(p,j+1);
                p[i].choosen=false;
        }
    }
}

long long int main()
{
    vector<arr> p;

    long long int N;
    cin>>N;
    arr i;
    while(N--)
    {
        cin>>i.value;
        i.choosen=false;
        p.push_back(i);
    }
    vector<long long int> o(p.size());
    permute(p,0);

}
