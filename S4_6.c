#include <stdio.h>

/* function to prlong long int the array in reverse order
 * using recursion */

void reverse(char a[])
{
  if(a[1]!='\0')
    reverse(a+1);
  prlong long intf("%c\t",a[0]);
}

long long int main(long long int argc, char *argv[])
{
  for(long long int i=0;i<argc;i++)
  {
    reverse(argv[i]);
    prlong long intf("\n");
  }
}
