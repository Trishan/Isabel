#include "iostream"
using namespace std;

void merge(long long int a1[],long long int a2[], long long int s1, long long int s2)
{
  long long int arr[s1+s2];
  long long int st1=0;
  long long int st2=0;

  for(long long int i=0;i<s1+s2;i++)
  {
    if(st1>s1-1) arr[i]=a2[st2++];
    else if(st2>s2-1) arr[i]=a1[st1++];
    else if(a1[st1]>a2[st2]) arr[i]=a1[st1++];
    else arr[i]=a2[st2++];
  }

  for(long long int i=0; i<s1+s2;i++)
    cout<<arr[i]<<" ";
}


long long int main()
{
  long long int T;
  cin>>T;
  while(T-->0)
  {
    long long int s1,s2;
    cin>>s1>>s2;
    long long int a1[s1],a2[s2];

    for(long long int i=0;i<s1;i++)
    {
      cin>>a1[i];
    }
    for(long long int i=0;i<s2;i++)
    {
      cin>>a2[i];
    }

    merge(a1,a2,s1,s2);

    cout<<"\n";

  }
}
