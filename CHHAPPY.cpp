#include <iostream>
#include <vector>
using namespace std;

long long int main()
{
    long long int T;
    cin>>T;
    while(T-->0)
    {
        long long int N;
        cin>>N;
        vector<vector<long long int>> arr(N);

        long long int var;
        for(long long int i=0;i<N;i++)
        {
            cin >> var;
            arr[var-1].push_back(i);
        }

        /*for(long long int i=0;i<N;i++)
        {
            cout<<i<<"::\t";
            for(long long int j=0;j<arr[i].size();j++)
                cout<<arr[i][j]<<"\t";
            cout<<"\n";
        }*/

        bool found =false;
        for(long long int i=0;i<N;i++)
        {
            if(arr[i].size()>1)
            {
                long long int pair=0;
                for(long long int j=0;j<arr[i].size();j++)
                {
                    if(arr[arr[i][j]].size()>0) pair++;
                    if(pair>=2)
                    {
                        found=true;
                        break;
                    }
                }
            }
            if(found) break;
        }
        if(found) cout<<"Truly Happy\n";
        else cout<<"Poor Chef\n";
    }
}
