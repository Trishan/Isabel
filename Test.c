#include <stdio.h>

#define diff(x,y) x>y?x-y:y-x
#define  sq(z) z*z

long long int main()
{
  prlong long intf("%d\n",sq(2+2));
  prlong long intf("%d\n",sq(3+3));
  prlong long intf("%d\n",diff(8,15) );
  prlong long intf("%d\n",8>15?8-15:15-8);
  prlong long intf("%d\n",diff(sq(2+2),sq(3+3)));
}
