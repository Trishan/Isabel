#include <stdio.h>
#include <stdlib.h>
/*
Characteristics::
There are five parts to this data structure:
a) A part which holds a Character.
b) Part which tells you whether it's the end of a string or not
c) Part which polong long ints to a node containing a character with value less than the preceding character
d) Part which polong long ints to a node containing a character with value equal than the preceding character
e) Part which polong long ints to a node containing a character with value more than preceding character
*/

struct Node
{
  char ch;
  long long int EndOfString;
  struct Node *lf;
  struct Node *eq;
  struct Node *rt;
};

/*
Creation::
a) We will define a polong long inter polong long inting to the node variable
b) The first such polong long inter will be the head and should be kept in record
c) We will insert the characters in ch and increment the count using member access operator
d) In every other part we will store the memory address of the subsequent nodes
e) We will insert a character only when we see the last polong long inter is NULL
 */

struct Node* newnode(char c)
{
  struct Node (*temp) = (struct Node *)malloc(sizeof(struct Node));
  temp->ch=c;
  temp->EndOfString=0;
  temp->lf=temp->eq=temp->rt=NULL;
  return temp;
}

/*
Insertion::
a) First we need to check whether the root  polong long inter id is polong long inting to NULL or not
b? If true, we need to create a node
c) If not, we will insert the next character according to Characteristics of Characteristics
d) Clearly we will be using recursion here.
e) If we find the root to be occupied and we have reach EndOfString, we'll set the EndOfString to 1;
*/

void insert(struct Node **head, char *s)
{
  if((*head)==NULL) *head=newnode(*s);
  if((*s)<((*head)->ch)) insert(&((*head)->lf), s);
  else if((*s)>((*head)->ch)) insert(&((*head)->rt), s);
  else
  {
    /*
    Here if the preceding character is same as the node then check if it's a NULL character
    used to terminate the string, if true then increment the EndOfString to 1
    if not go the next character of the string
    */
    if(*(s+1)!='\0') insert(&((*head)->eq),s+1);
    else (*head)->EndOfString++;
  }
}

void prlong long int_tree(struct Node *head,char *buffer,long long int depth)
{
  if(head!=NULL)
  {
    prlong long int_tree(head->lf,buffer,depth);
    buffer[depth]=head->ch;
    if(head->EndOfString)
    {
      buffer[depth+1]='\0';
      prlong long intf("%s",buffer);
    }
    prlong long int_tree(head->eq,buffer,depth+1);
    prlong long int_tree(head->rt,buffer,depth);
  }
}

long long int main(void)
{
  struct Node *head=NULL;
  char line[100][20];
  char buffer[20];


long long int i=0;
while(line[i-1][0]!='\n')
{
  fgets(line[i],100,stdin);
  i++;
}
while(--i>-1)
  insert(&head,line[i]);

  prlong long int_tree(head,buffer,0);



}
